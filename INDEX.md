# Nansi

An ANSI driver for DOS


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## NANSI.LSM

<table>
<tr><td>title</td><td>Nansi</td></tr>
<tr><td>version</td><td>4.0d (2007may26) (rev A)</td></tr>
<tr><td>entered&nbsp;date</td><td>2007-05-26</td></tr>
<tr><td>description</td><td>An ANSI driver for DOS</td></tr>
<tr><td>keywords</td><td>freedos, nansi, ansi</td></tr>
<tr><td>author</td><td>Dan Kegel &lt;dank@kegel.com&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Eric Auer &lt;eric@CoLi.Uni-SB.DE&gt;</td></tr>
<tr><td>alternate&nbsp;site</td><td>http://www.ibiblio.org/pub/micro/pc-stuff/freedos/files/dos/nansi/</td></tr>
<tr><td>original&nbsp;site</td><td>http://www.kegel.com/</td></tr>
<tr><td>platforms</td><td>dos</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
</table>
